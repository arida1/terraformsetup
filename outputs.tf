output "gke_cluster_name" {
  value       = module.gke.name
  description = "GKE Cluster Name"
}

output "cluster_endpoint" {
    value = module.gke.endpoint
    description = "Cluster Endpoint"
    sensitive = true
}

output "instance_groups" {
    value = module.gke.instance_group_urls
    description = "Instance Group URLs"
}