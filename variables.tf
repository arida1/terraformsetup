variable "region" {
  type        = string
  description = "Name of the Region"
  default     = "us-central1" ## London
}

variable "zone_a" {
  type        = string
  description = "Zone A"
  default     = "us-central1-a" ## London
}

variable "zone_b" {
  type        = string
  description = "Zone B"
  default     = "us-central1-b" ## London
}

variable "zone_c" {
  type        = string
  description = "Zone C"
  default     = "us-central1-c" ## London
}

variable "project" {
  type        = string
  description = "Name of the Project"
  default     = "demoproject-319207"
}

variable "public_subnet_cidr" {
  type        = string
  description = "Public Subnet CIDR"
  default = "10.10.0.0/16"
}

variable "private_subnet_cidr" {
  type        = string
  description = "Public Subnet CIDR"
  default = "10.20.0.0/16"
}

#GKE cluster

variable "k8s_version" {
  default = "1.18.17-gke.1901"
  description = "Kubernetes version"
  type = string
}

variable "ip_range_pods_name" {
  default = "subnet-01-pods"
  description= "Subnet range for pods"
  type = string
}

variable "ip_range_services_name" {
  default = "subnet-01-services"
  description= "Subnet range for services"
  type = string
}
