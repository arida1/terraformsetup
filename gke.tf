data "google_client_config" "default" {}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version                    = "15.0.1"
  project_id                 = var.project
  name                       = "${var.project}-gke-cluster"
  regional                   = false
  region                     = var.region
  zones                      = [var.zone_a, var.zone_b, var.zone_c]
  network                    = module.vpc.network_name
  subnetwork                 = "${var.project}-private-subnet"   ##module.vpc.subnets_names[0]
  ip_range_pods              =  var.ip_range_pods_name
  ip_range_services          =  var.ip_range_services_name
  kubernetes_version         = var.k8s_version
  network_policy             = false
  enable_private_endpoint    = false
  enable_private_nodes       = true
  # release_channel          = "STABLE"
  remove_default_node_pool   = true
  horizontal_pod_autoscaling = true

  master_authorized_networks = [{
    cidr_block = "114.143.222.74/32"
    display_name = "Webonise office IP"
  },
  {
    cidr_block = "103.110.253.253/32"
    display_name = "Monali Home"
  }
  ]

  # cluster_autoscaling = {
  #   enabled             = true
  #   autoscaling_profile = "BALANCED"
  #   max_cpu_cores       = 0
  #   min_cpu_cores       = 0
  #   max_memory_gb       = 0
  #   min_memory_gb       = 0
  # }

  node_pools = [
    {
      name                      = "${var.project}-std-np"
      machine_type              = "n2-standard-2"
      node_locations            = "${var.zone_a},${var.zone_b},${var.zone_c}"
      min_count                 = 0
      max_count                 = 5
      disk_size_gb              = 30
      preemptible               = true
      version                   = var.k8s_version
      initial_node_count        = 0
    },
    ## These machine types give issues with certain zones, need to check
    {
      name                      = "${var.project}-highmem-np"
      machine_type              = "n2-highmem-80"
      node_locations            = "${var.zone_a},${var.zone_b},${var.zone_c}"
      min_count                 = 0
      max_count                 = 2
      disk_size_gb              = 30
      preemptible               = true
      version                   = var.k8s_version
      initial_node_count        = 0
    },
    {
      name                      = "${var.project}-highgpu-np"
      machine_type              = "a2-highgpu-1g"
      node_locations            = "${var.zone_a},${var.zone_b},${var.zone_c}"
      min_count                 = 0
      max_count                 = 2
      disk_size_gb              = 30
      preemptible               = true
      version                   = var.k8s_version
      initial_node_count        = 0
    }
  ]

  node_pools_tags = {
    all = [
      "test",
      "node"
    ]

  }
}

## configure authentication to a GKE cluster 
module "gke_auth" {
  source = "terraform-google-modules/kubernetes-engine/google//modules/auth"
  version = "15.0.1"
  depends_on   = [module.gke]
  project_id   = var.project
  location     = module.gke.location
  cluster_name = module.gke.name
}

## Create a local kubeconfig file
resource "local_file" "kubeconfig" {
  content  = module.gke_auth.kubeconfig_raw
  filename = "kubeconfig-test"
}