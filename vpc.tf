## Launch a GCP network using module
# https://github.com/terraform-google-modules/terraform-google-network

module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "3.0"

  project_id   = var.project
  network_name = "${var.project}-vpc"
  routing_mode = "REGIONAL"

  //delete_default_internet_gateway_routes = "true"

  subnets = [
    {
      subnet_name           = "${var.project}-public-subnet"
      subnet_ip             = var.public_subnet_cidr
      subnet_region         = var.region
      subnet_private_access = "false"
      subnet_flow_logs      = "false"
    },
    {
      subnet_name           = "${var.project}-private-subnet"
      subnet_ip             = var.private_subnet_cidr
      subnet_region         = var.region
      subnet_private_access = "true"
      subnet_flow_logs      = "false"
    }
  ]
  secondary_ranges = {
    "${var.project}-private-subnet" = [
      {
        range_name    = var.ip_range_pods_name
        ip_cidr_range = "192.168.64.0/24"
      },
      {
        range_name    = var.ip_range_services_name
        ip_cidr_range = "192.168.74.0/24"
      }      
    ]
  }

  routes = [
    {
      name              = "${var.project}-egress-internet"
      description       = "Default route through IGW to access internet"
      destination_range = "0.0.0.0/0"

      next_hop_internet = "true"
    }
  ]
}

## Setup a cloud NAT to allow private vms to connect to internet
# https://github.com/terraform-google-modules/terraform-google-cloud-router

module "cloud_router" {
  source  = "terraform-google-modules/cloud-router/google"
  version = "1.1.0"
  name    = "${var.project}-router"
  depends_on   = [module.vpc]
  project = var.project
  region  = var.region
  network = module.vpc.network_name
  nats = [{
    name                               = "${var.project}-nat"
    nat_ip_allocate_option             = "AUTO_ONLY"
    source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
    subnetworks = [{
      name                    = "${var.project}-private-subnet"
      source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
    }]
  }]
}
