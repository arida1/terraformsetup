provider "google" {
    credentials = file("demoproject.json")
    region   = var.region
    project  = var.project
    //zone     =  local.zone 
}

# provider "google-beta" {
#     credentials = "${file("webonise-devops.json")}"
#     region   = var.region
#     project  = var.project
#     //zone     =  local.zone
# }